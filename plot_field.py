import xarray as xr
import numpy as np
import  matplotlib.pyplot as plt
plt.switch_backend('agg')

ds=xr.open_dataset('output/pcp1950.nc')
f=ds["var"].values[30,:,:]

plt.imshow(f,vmin=0.,vmax=np.amax(f))

plt.savefig("bucket_mount/test.png")
