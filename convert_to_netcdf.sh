#!/bin/bash

for y in {2016..2017}; do
	# Convert all daily fields to netcdf (zero-pad netcdf names)
	sd="$y"

	for d in {1..366}; do #Loop over 366 days.  For non leap years, day 60 is skipped
	    for v in pcp;do
		dlong=`printf "%03d" $d`
		fascii="$v""$y"_"$d".asc
		fncdf="$v""$y"_"$dlong".nc
		echo Converting $fascii to $fncdf
		gdal_translate $sd/$fascii -of GMT $sd/$fncdf #Use gdal to convert ascii to netCDF.  Faster to do with gdal, and gdal also auto-accounts for header info.
                rm -v $sd/$fascii #Remove ascii file (comment out if you want to keep for redundancy
	    done
	done
done

