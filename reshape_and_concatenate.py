import xarray as xr
import numpy as np

#for yr in range(1950,2016): #Loop over all years
for yr in range(2016,2018): #Loop over final few years
    print("Processing year: "+str(yr))
    #for var in ["min","max","pcp"]: #Loop over all variables
    for var in ["min","max"]: #Loop over just precip
        fnamesIn=str(yr)+"/"+var+"*_*.nc" #Define input file name
        print("Loading daily vector data for: "+var+"...")
        arr=xr.open_mfdataset(fnamesIn,concat_dim="day") #Use xarray open_mfdataset to open all daily fields into one yearly matrix.
        v=arr["z"].values #Extract actual data values from DataArray
        print("Reshaping data...")
        vs=np.shape(v)
        v=np.reshape(v,(vs[0],510,1068)) #Make new gridded array.  In first dimension, account for changing size due to leap year occurrence
        print("Saving yearly data to DataArray...")
        fnameOut="output/"+var+str(yr)+".nc"
        OutArray=xr.DataArray(v,dims=["day","y","x"],name="var") #Make new DataArray that holds gridded dataset.  Define some dimension names.  More metadata could be added here
        print("Saving to file...")
        OutArray.to_netcdf(fnameOut,mode='w') #Write to file
        del OutArray
        del v
        del arr
        print("Done...")
